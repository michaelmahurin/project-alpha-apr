from django.shortcuts import get_object_or_404, render, redirect
from .forms import Taskform, CompletedTaskForm
from .models import Task
from django.contrib.auth.decorators import login_required

# Create your views here.


def create_task(request):
    context = {}
    form = Taskform(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("show_project")
    context["form"] = form
    return render(request, "taskthings/create.html", context)


@login_required
def show_my_tasks(request):
    task_list = Task.objects.filter(assignee=request.user)
    context = {"tasklist": task_list}
    return render(request, "taskthings/list.html", context)


def complete_task(request, pk):
    created = get_object_or_404(Task, pk=pk)
    form = CompletedTaskForm(request.POST or None, instance=created)
    if form.is_valid():
        form.save()
        return redirect("show_my_tasks")
    context = {"form": form}
    return render(request, "taskthings/list.html", context)
