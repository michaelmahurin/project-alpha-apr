from django.urls import  path
from tasks.views import show_my_tasks, complete_task, create_task


urlpatterns = [
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("<int:pk>/complete/", complete_task, name="complete_task"),
    path("create/", create_task, name="create_task"),
]
