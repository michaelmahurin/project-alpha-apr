from django import forms
from .models import Task


class Taskform(forms.ModelForm):
    class Meta:
        model = Task
        fields = ["name", "start_date", "due_date", "project", "assignee"]


class CompletedTaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ["is_completed"]
