# Generated by Django 4.0.6 on 2022-08-03 23:08

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("tasks", "0002_alter_task_project"),
    ]

    operations = [
        migrations.RenameField(
            model_name="task",
            old_name="is_complete",
            new_name="is_completed",
        ),
    ]
