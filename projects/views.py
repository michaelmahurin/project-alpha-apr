from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.form import ProjectForm

# Create your views here.


@login_required
def list_projects(request):
    project_list = Project.objects.filter(members=request.user)
    context = {"project_list": project_list}
    return render(request, "things/list.html", context)


@login_required
def show_project(request, pk):
    context = {
        "project": Project.objects.get(pk=pk) if Project else None,
    }
    return render(request, "things/details.html", context)


@login_required
def create_project(request):
    context = {}
    form = ProjectForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = request.user
        return redirect("show_project", form.id)
    context["form"] = form
    return render(request, "things/create.html", context)
